<?php

if (!function_exists('enviaEmail')) {

  function enviaEmail($dados) {

    if (is_array($dados)) {

      $emailDestinatario = isset($dados['email']) ? $dados['email'] : 'ti@grupompe.com.br';
      $nomeDestinatario = isset($dados['nome']) ? $dados['nome'] : 'Administrador do Sistema';
      $titulo = isset($dados['titulo']) ? $dados['titulo'] : 'Intranet Grupo MPE';
      $assunto = isset($dados['assunto']) ? $dados['assunto'] : 'Intranet Grupo MPE';
      $mensagem = isset($dados['mensagem']) ? $dados['mensagem'] : 'Mensagem do Grupo MPE';

      $mail = new \PHPMailer();
      $mail->IsSMTP();

      $mail->CharSet = 'UTF-8';
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'ssl';
      $mail->Host = 'mpemta.grupompe.com.br';
      $mail->Port = 465;
      $mail->Username = 'gestaodepessoas';
      $mail->Password = 'alterar1';
      $mail->IsHTML(TRUE);
      $mail->WordWrap = TRUE;
      $mail->SetFrom('noreply@grupompe.com.br');
      $mail->AddReplyTo('noreply@grupompe.com.br', 'noreply - ' . $titulo);
      $mail->Subject = $assunto;
      $mail->AddAddress($emailDestinatario, $nomeDestinatario);

      $inst = &get_instance();
      $corpoEmail = $inst->load->view('email', '', TRUE);

      $corpoEmail = str_replace('@@title@@', $titulo, $corpoEmail);

      $corpoEmail = str_replace('@@content@@', $mensagem, $corpoEmail);
      $mail->Body = $corpoEmail;
      

      /* Também é possível adicionar anexos. */
//$mail->AddAttachment("images/phpmailer.gif");
//$mail->AddAttachment("images/phpmailer_mini.gif");

      if (!$mail->Send()) {
        throw new Exception('show_stack_bar_top("error", "Erro no envio de e-mail!", "Ocorreu um erro durante o envio: ' . $mail->ErrorInfo . '");');
      } else {
        return TRUE;
      }
    } else {
      throw new Exception('show_stack_bar_top("error", "Erro dados!", "Erro no envio dos dados para o e-mail!<br>Envie array!!!");');
    }
  }

}

