<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?php echo FILE_PUBLIC; ?>assets/css/main.css">
    <script src="<?php echo FILE_PUBLIC; ?>assets/js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo FILE_PUBLIC; ?>assets/icons/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <nav class="sidenav left" role="navigation">
      <ul class="menu">
        <li class="user">
          <!--
          <div class="search-block">
            <input type="text" class="search">
              <span class="">Search <i class="fa fa-search"></i></span>
            </div>
          -->
          <div class="media">
            <img class="img-circle" height="54" src="<?php echo $icon_user; ?>" alt="<?php echo $icon_user; ?>">
            <div class="media-body">
              <p><?php echo $funcionario[model_funcionario::NOME]; ?>
                <br>
                <small><?php echo $funcionario[model_funcionario::EMAIL]; ?></small>
              </p>
            </div>
          </div>
        </li>
        <?php foreach ($menu_principal as $row): ?>
          <li>
            <a href="<?php echo site_url($row[model_modulo::URL]); ?>" >
              <?php echo $row[model_modulo::NOME]; ?>
              <div>
                <i class="<?php echo $row[model_modulo::ICON]; ?> iconsMenuPrincipal"></i>
              </div>
            </a>
          </li>
        <?php endforeach; ?>

        <li class="logout" style="position: relative; border-top-width: 0px;">
          <a href="<?php echo site_url('nucleo/login/logoff'); ?>">
            Logout
            <div class="pull-right">
              <i class="fa fa-power-off iconsMenuPrincipal"></i>
            </div>
          </a>
        </li>
      </ul>
    </nav>
    <section class="legend">
      <div class="overlay"></div>
      <div class="controlshint">
        <img src="<?php echo FILE_PUBLIC; ?>assets/img/swipe.png" alt="Menu Help"></div>
    </section>
    <div id="overlay">
      <!--<span><i class="fa fa-spinner fa-spin"></i> Carregando...</span>-->
      <div class="loading">
        <div class="loading_cube">
          <div class="cube_big">
            <div class="cube_small"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrap">
