<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_modulo
 *
 * @author denner.fernandes
 */
class model_modulo extends MY_Model {

  const TABELA = 'MODULO';
  const ID = 'ID_MODULO';
  const NOME = 'DS_DESCRICAO';
  const ICON = 'DS_ICON';
  const URL = 'DS_URL';

  public function __construct() {
    parent::__construct();
  }

  public function __destruct() {
    
  }

}
