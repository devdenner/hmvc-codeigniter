<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_acesso
 *
 * @author denner.fernandes
 */
class model_acesso extends MY_Model {

  const TABELA = 'ACESSO';
  const ID = 'ID_ACESSO';
  const USUARIO = 'ID_USUARIO';
  const MODULO = 'ID_MODULO';
  const PRIVILEGIO = 'CD_PRIVILEGIO';

  public function __construct() {
    parent::__construct();
  }

  public function getByUsuario($busca = NULL, $page = NULL, $paginacao = NULL) {

    try {
      $this->db->cache_on();
      $return = NULL;

      $this->db->select('AC.' . self::ID . ', FU.' . model_funcionario::NOME . ' NOME, 
              US.' . model_usuario::AD . ' USUARIO, MO.' . model_modulo::NOME . ' MODULO, 
              PR.' . model_privilegio::DESCRICAO . ' PRIVILEGIO');
      $this->db->join(model_usuario::TABELA . ' US', 'US.' . model_usuario::ID . ' = AC.' . self::USUARIO);
      $this->db->join(model_funcionario::TABELA . ' FU', 'FU.' . model_funcionario::ID . ' = US.' . model_usuario::FUNC);
      $this->db->join(model_modulo::TABELA . ' MO', 'MO.' . model_modulo::ID . ' = AC.' . self::MODULO);
      $this->db->join(model_privilegio::TABELA . ' PR', 'PR.' . model_privilegio::ID . ' = AC.' . self::PRIVILEGIO);
      if (!is_null($busca)) {
        $this->db->like('FU.' . model_funcionario::NOME, $busca);
        $this->db->or_like('US.' . model_usuario::AD, $busca);
      }
      $this->db->order_by(self::ID, "ASC");

      if (!is_null($page) && !is_null($paginacao)) {
        $query = $this->db->get(self::TABELA . ' AC', $page, $paginacao);
      } else {
        $query = $this->db->get(self::TABELA . ' AC');
      }

      $return = $query->result_array();
      if (!is_null($return)) {
        return $return;
      } else {
        throw new Exception('show_stack_bar_top("error", "Erro", "Não há registros.")');
      }
    } catch (Exception $exc) {
      return $exc->getMessage();
    }
  }

  public function getMenu($ID) {
    try {
      $this->db->cache_on();
      $this->db->select('MO.' . model_modulo::NOME . ', MO.' . model_modulo::ICON . ', MO.' . model_modulo::URL);
      $this->db->join(model_modulo::TABELA . ' MO', 'MO.' . model_modulo::ID . ' = AC.' . self::MODULO);
      $this->db->where('AC.' . self::USUARIO, $ID);
      $this->db->order_by('AC.' . self::ID, "ASC");
      $query = $this->db->get(self::TABELA . ' AC');

      if ($query->num_rows > 0) {
        return $query->result_array();
      } else {
        throw new Exception('show_stack_bar_top("error", "Erro", "Não há registros para o menu.")');
      }
    } catch (Exception $exc) {
      return $exc->getMessage();
    }
  }

  public function __destruct() {
    
  }

}
