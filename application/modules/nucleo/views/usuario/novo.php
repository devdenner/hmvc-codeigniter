<form id="formulario" class="form-horizontal" role="form" method="post" action="<?php echo base_url('nucleo/usuario/cadastrar'); ?>" onsubmit="overlay(true)">
  <fieldset class="col-sm-6 col-sm-offset-3 well">
    <div class="header">Novo Usuário</div>
    <div class="form-group">
      <label for="ad" class="col-sm-3 control-label">Login</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="ad" name="<?php echo model_usuario::AD; ?>" placeholder="Login para entrada no sistema" required autofocus />
      </div>
    </div>
    <div class="form-group">
      <label for="senha" class="col-sm-3 control-label">Senha</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="senha" name="<?php echo model_usuario::SENHA; ?>" placeholder="Senha para a entrada no sistema" required />
      </div>
    </div>
    <div class="form-group">
      <label for="email" class="col-sm-3 control-label">E-mail</label>
      <div class="col-sm-9">
        <input type="email" class="form-control" id="email" name="<?php echo model_funcionario::EMAIL; ?>" placeholder="E-mail do usuário" required />
      </div>
    </div>
    <div class="form-group">
      <label for="empresa" class="col-sm-3 control-label">Empresa</label>
      <div class="col-sm-9">
        <select class="form-control" id="empresa" name="<?php echo model_funcionario::EMPRESA; ?>" required>
          <option value="">Escolha uma opção</option>
          <option value="1">MPE</option>
          <option value="2">EBE</option>
          <option value="3">GEMON</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="chapa" class="col-sm-3 control-label">Chapa</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="chapa" name="<?php echo model_funcionario::CHAPA; ?>" placeholder="Chapa do usuário" required />
      </div>
    </div>
    <div class="form-group">
      <label for="cpf" class="col-sm-3 control-label">CPF</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="cpf" name="<?php echo model_funcionario::CPF; ?>" placeholder="CPF do usuário" required />
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <div class="m-btn-group pull-right">
          <button type="reset" class="m-btn" onclick="javascript:history.back()">Cancelar</button>
          <button type="submit" class="m-btn blue">Cadastrar</button>
        </div>
      </div>
    </div>
  </fieldset>
</form>