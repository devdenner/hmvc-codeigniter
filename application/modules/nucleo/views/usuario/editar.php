<form id="formulario" class="form-horizontal" role="form" method="post" action="<?php echo base_url('nucleo/usuario/atualizar'); ?>" onsubmit="overlay(true)">
  <fieldset class="col-sm-6 col-sm-offset-3 well">
    <div class="header">Editar Usuário</div>
    <input type="hidden" value="<?php echo $usuario[model_usuario::ID]; ?>" name="<?php echo model_usuario::ID; ?>" />
    <input type="hidden" value="<?php echo $usuario[model_funcionario::ID]; ?>" name="<?php echo model_funcionario::ID; ?>" />
    <div class="form-group">
      <label for="ad" class="col-sm-3 control-label">Login</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="ad" name="<?php echo model_usuario::AD; ?>" placeholder="Login para entrada no sistema" value="<?php echo $usuario[model_usuario::AD]; ?>" disabled />
      </div>
    </div>
    <div class="form-group">
      <label for="senha" class="col-sm-3 control-label">Senha</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="senha" name="<?php echo model_usuario::SENHA; ?>" placeholder="Digite uma nova senha" />
      </div>
    </div>
    <div class="form-group">
      <label for="email" class="col-sm-3 control-label">E-mail</label>
      <div class="col-sm-9">
        <input type="email" class="form-control" id="email" name="<?php echo model_funcionario::EMAIL; ?>" placeholder="E-mail do usuário" required value="<?php echo $usuario[model_funcionario::EMAIL]; ?>" />
      </div>
    </div>
    <div class="form-group">
      <label for="empresa" class="col-sm-3 control-label">Empresa</label>
      <div class="col-sm-9">
        <select class="form-control" id="empresa" name="<?php echo model_funcionario::EMPRESA; ?>" required>
          <option value="">Escolha uma opção</option>
          <option value="1" <?php echo $usuario[model_funcionario::EMPRESA] == 1 ? 'selected' : ''; ?>>MPE</option>
          <option value="2" <?php echo $usuario[model_funcionario::EMPRESA] == 2 ? 'selected' : ''; ?>>EBE</option>
          <option value="3" <?php echo $usuario[model_funcionario::EMPRESA] == 3 ? 'selected' : ''; ?>>GEMON</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="chapa" class="col-sm-3 control-label">Chapa</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="chapa" name="<?php echo model_funcionario::CHAPA; ?>" placeholder="Chapa do usuário" required value="<?php echo $usuario[model_funcionario::CHAPA]; ?>"/>
      </div>
    </div>
    <div class="form-group">
      <label for="cpf" class="col-sm-3 control-label">CPF</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="cpf" name="<?php echo model_funcionario::CPF; ?>" placeholder="CPF do usuário" required value="<?php echo $usuario[model_funcionario::CPF]; ?>" />
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <div class="m-btn-group pull-right">
          <button type="reset" class="m-btn" onclick="javascript:history.back()">Cancelar</button>
          <button type="submit" class="m-btn blue">Atualizar</button>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Outras Informações</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <dl class="col-sm-6">
            <dt>ADMISSÃO:</dt>
            <dd><?php echo $usuario[model_funcionario::ADMISSAO]; ?></dd>
            <dt>FUNÇÃO:</dt>
            <dd><?php echo $usuario[model_funcionario::FUNCAO]; ?></dd>
            <dt>SEÇÃO:</dt>
            <dd><?php echo $usuario[model_funcionario::SECAO]; ?></dd>
          </dl>
          <dl class="col-sm-6">
            <dt>DESCRIÇÃO SEÇÃO:</dt>
            <dd><?php echo $usuario[model_funcionario::DESCSECAO]; ?></dd>
            <dt>SITUAÇÃO:</dt>
            <dd><?php echo $usuario[model_funcionario::SITUACAO]; ?></dd>
            <dt>TIPO:</dt>
            <dd><?php echo $usuario[model_funcionario::TIPO]; ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </fieldset>
</form>