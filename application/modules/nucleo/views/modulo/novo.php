<form id="formulario" class="form-horizontal" role="form" method="post" action="<?php echo base_url('nucleo/modulo/cadastrar'); ?>" onsubmit="overlay(true)">
  <fieldset class="col-sm-6 col-sm-offset-3 well">
    <div class="header">Novo Modulo</div>
    <div class="form-group">
      <label for="nome" class="col-sm-3 control-label">Nome</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="nome" name="<?php echo model_modulo::NOME; ?>" placeholder="Nome do Módulo" required autofocus />
      </div>
    </div>
    <div class="form-group">
      <label for="icon" class="col-sm-3 control-label">Ícone</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="icon" name="<?php echo model_modulo::ICON; ?>" placeholder="Ícone do Módulo" required />
      </div>
    </div>
    <div class="form-group">
      <label for="url" class="col-sm-3 control-label">URL</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="url" name="<?php echo model_modulo::URL; ?>" placeholder="URL do Módulo" required />
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <div class="m-btn-group pull-right">
          <button type="reset" class="m-btn" onclick="javascript:history.back()">Cancelar</button>
          <button type="submit" class="m-btn blue">Cadastrar</button>
        </div>
      </div>
    </div>
  </fieldset>
</form>