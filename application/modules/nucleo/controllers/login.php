<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class login extends MY_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    $this->load->view('login/index', $this->data);
    $this->load->view('alerts', $this->data);
  }

  public function logar() {
    try {

      if (isset($this->POST)) {
        if (!empty($this->POST['chapa']) && !empty($this->POST['senha'])) {

          $this->validar(TRUE);

          $this->load->helper('security');

          $criterio = array(
              model_funcionario::CHAPA => str_pad($this->POST['chapa'], 6, '0', STR_PAD_LEFT),
              model_usuario::SENHA => do_hash($this->POST['senha']),
          );

          $usuario = $this->model_usuario->getUsuario($criterio);

          if (empty($usuario)) {
            throw new Exception('show_stack_bar_top("error", "Login e/ou Senha não encontradas!", "Falha no Login");');
          } else {
            if ($usuario[0][model_funcionario::SITUACAO] == 'D') {
              throw new Exception('show_stack_bar_top("error", "Usuário Inativo!", "Usuário sem acesso ao sistema.");');
            }
            $token = $this->model_usuario->setToken($usuario[0][model_usuario::ID]);
            $this->session->set_userdata(parent::session_usu, $token);
            redirect('cadastro');
          }
        } else {
          throw new Exception('show_stack_bar_top("error", "Digite seu Login e Senha!", "Falha no Login");');
        }
      }
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
      redirect('nucleo/login');
    }
  }

  public function logoff() {
    $this->session->sess_destroy();
    redirect('nucleo/login');
  }

  public function registrar() {

    $this->load->view('login/registro', $this->data);
    $this->load->view('alerts', $this->data);
  }

  public function realizaRegistro() {

    try {

      $this->validar();

      $this->load->Model('model_login');
      $this->load->helper('security');

      $chapa = str_pad($this->POST['chapa'], 6, '0', STR_PAD_LEFT);
      $email = strip_quotes($this->POST['email']) . '@grupompe.com.br';
      $senha = do_hash($this->POST['senha']);
      $cpf = str_replace('.', '', str_replace('-', '', $this->POST['cpf']));
      $dataadm = $this->POST['dataadm'];
      $empresa = $this->POST['empresa'];
      $dados = $this->model_login->getUsuario($chapa, $cpf, $dataadm)[0];

      if (empty($dados)) {
        throw new Exception('Chapa, CPF e Data de Admissão são de um usuário válido.');
      } else {
        $idFunc = $this->model_funcionario->autoincrement();
        $dados[model_funcionario::ID] = $idFunc;
        $dados[model_funcionario::EMAIL] = $email;
        $dados[model_funcionario::EMPRESA] = $empresa;
        settype($dados[model_funcionario::ID], 'integer');

        //Inicio do transact
        $this->db->trans_start();

        $resposta = $this->model_funcionario->save($dados);

        $dados = array(
            model_usuario::ID => $this->model_usuario->autoincrement(),
            model_usuario::SENHA => $senha,
            model_usuario::FUNC => $idFunc,
            model_usuario::STATUS => 'A'
        );

        $resposta2 = $this->model_usuario->save($dados);

        //Preparando cadastro no módulo default
        $this->load->Model('model_acesso');

        $dados = array(
            model_acesso::ID => $this->model_acesso->autoincrement(),
            model_acesso::USUARIO => $idFunc,
            model_acesso::MODULO => 1,
            model_acesso::PRIVILEGIO => '01'
        );

        settype($dados[model_acesso::ID], 'integer');

        $resposta3 = $this->model_acesso->save($dados);

        //Final do transact
        $this->db->trans_complete();

        if ($resposta && $resposta2 && $resposta3) {
          $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Cadastro realizado com sucesso.")');
        } else {
          throw new Exception('Houve algum erro no seu cadastro.');
        }
      }
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', 'show_stack_bar_top("error", "Erro", "' . $exc->getMessage() . '")');
    }
    redirect('nucleo/login');
  }

  private function validar($logar = FALSE) {

    if (empty($this->POST['chapa'])) {
      throw new Exception('Campo <b>Chapa</b> não pode ficar vazio.');
    }
    if (empty($this->POST['senha'])) {
      throw new Exception('Campo <b>Senha</b> não pode ficar vazio.');
    }
    if (!$logar) {
      if (!is_numeric($this->POST['chapa'])) {
        throw new Exception('Campo <b>Chapa</b> deve ser um valor numérico.');
      }
      if (!empty($this->model_funcionario->get(array(model_funcionario::CHAPA => $this->POST['chapa'])))) {
        throw new Exception('Matrícula ' . $this->POST['chapa'] . ' já cadastrada no sistema. Por favor entre em contato com o suporte.');
      }
      if (empty($this->POST['senha_confirm'])) {
        throw new Exception('Campo de confirmação de <b>Senha</b> não pode ficar vazio.');
      }
      if ($this->POST['senha'] != $this->POST['senha_confirm']) {
        throw new Exception('As <b>Senhas</b> não são idênticas.');
      }
      if (empty($this->POST['cpf'])) {
        throw new Exception('Campo <b>CPF</b> não pode ficar vazio.');
      }
      if (!is_numeric($this->POST['cpf'])) {
        throw new Exception('Campo <b>CPF</b> deve ser um valor numérico.');
      }
      if (empty($this->POST['dataadm'])) {
        throw new Exception('Campo <b>Data de Admissão</b> não pode ficar vazio.');
      }
      if (empty($this->POST['email'])) {
        throw new Exception('Campo <b>E-mail</b> não pode ficar vazio.');
      }
      if (!filter_var($this->POST['email'] . '@grupompe.com.br', FILTER_VALIDATE_EMAIL)) {
        throw new Exception('Campo <b>E-mail</b> não parece um e-mail valido. <br> 
                           Obs.: Não coloque o dominio do e-mail (@grupompe.com.br) <br>
                           Utilize seu login conforme no Zimbra.');
      }
      if (empty($this->POST['empresa'])) {
        throw new Exception('Escolha um opção do campo <b>Empresa</b>.');
      }
    }
  }

  public function esqueciMinhaSenha() {
    $this->load->view('login/esqueci', $this->data);
    $this->load->view('alerts', $this->data);
  }

  public function enviarEmail() {
    try {
      $email = $this->POST['email'];
      $funcionario = $this->model_funcionario->get(array(model_funcionario::EMAIL => $email))[0];

      $codEmail = base64_encode($funcionario[model_funcionario::EMAIL]);

      $mensagem = '<p class="text-center">Você solicitou a troca de sua senha de acesso a intranet do GRUPO MPE.<br />';
      $mensagem .= 'Clique no link a seguir para ser redirecionado a página de troca de senha.</p>';
      $mensagem .= '<a class="btn btn-link" href="' . site_url('nucleo/login/resetarSenha/?email=' . $codEmail) . '">Trocar Senha</a>';
      $mensagem .= '<p class="text-center">Se você não solicitou a troca de senha, envie um e-mail a ti@grupompe.com.br relatando o ocorrido.</p>';

      $dados = array(
          'nome' => $funcionario[model_funcionario::NOME],
          'email' => $funcionario[model_funcionario::EMAIL],
          'titulo' => 'Troca de e-mail',
          'assunto' => 'Troca de e-mail',
          'mensagem' => $mensagem,
      );
      $this->load->helper('functions');

      if (enviaEmail($dados)) {
        $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Você recebeu um link para trocar sua senha em seu e-mail cadastrado. Por favor, verifique sua caixa de entrada de seu e-mail.")');
        redirect('nucleo/login');
      }
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
      redirect('nucleo/esqueciMinhaSenha');
    }
  }

  public function resetarSenha() {
    try {
      $email = base64_decode($_GET['email']);
      
      
      
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
      $this->load->view('login/esqueci', $this->data);
      $this->load->view('alerts', $this->data);
    }
  }

  public function __destruct() {
    
  }

}
