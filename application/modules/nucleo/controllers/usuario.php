<?php

if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class usuario extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->data['menu_usuario'] = 'active';
  }

  public function index() {

    if ($this->POST) {

      $this->data['usuario'] = $this->model_usuario->getByNome($this->POST[model_usuario::AD]);
      $this->data['resultado'] = $this->POST[model_usuario::AD];
    }

    $this->data['menu_usuario'] = 'active';
    $this->data['breadcrumb'] = $this->breadcrumb(array('nucleo', 'usuario'), array('Núcleo', 'Usuário'));
    $this->MY_view('nucleo/usuario/listar', $this->data, 'nucleo/commom/header');
  }

  public function novo() {
    $this->data['breadcrumb'] = $this->breadcrumb(array('nucleo', 'usuario', 'novo'), array('Núcleo', 'Usuário', 'Novo'));
    $this->MY_view('nucleo/usuario/novo', $this->data, 'nucleo/commom/header');
  }

  public function cadastrar() {
    try {
      $this->validar();
      $this->load->Model('model_login');
      $this->load->helper('security');
      $busca = $this->model_login->getUsuario($this->POST[model_funcionario::CHAPA], $this->POST[model_funcionario::CPF])[0];

      if (is_array($busca)) {

        $campos = array(
            model_funcionario::NOME,
            model_funcionario::CHAPA,
            model_funcionario::CPF,
            model_funcionario::ADMISSAO,
            model_funcionario::FUNCAO,
            model_funcionario::SECAO,
            model_funcionario::DESCSECAO,
            model_funcionario::SITUACAO,
            model_funcionario::TIPO
        );
        $dados = elements($campos, $busca);
        $dados[model_funcionario::ID] = $this->model_funcionario->autoincrement();
        $dados[model_funcionario::EMAIL] = $this->POST[model_funcionario::EMAIL];
        $dados[model_funcionario::EMPRESA] = $this->POST[model_funcionario::EMPRESA];

        $campos2 = array(model_usuario::AD, model_usuario::SENHA);
        $dados2 = elements($campos2, $this->POST);
        $dados2[model_usuario::ID] = $this->model_usuario->autoincrement();
        $dados2[model_usuario::SENHA] = do_hash($dados2[model_usuario::SENHA]);
        $dados2[model_usuario::STATUS] = 'A';
        $dados2[model_usuario::FUNC] = $dados[model_funcionario::ID];
      } else {
        throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não encontrado.")');
      }

      $acao = $this->model_funcionario->save($dados);
      $acao2 = $this->model_usuario->save($dados2);

      if ($acao && $acao2) {
        $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Usuário cadastrado com sucesso.")');
      } else {
        throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não cadastrado.")');
      }
      redirect('nucleo/usuario');
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
      redirect('nucleo/usuario/novo');
    }
  }

  private function validar($pula_login = FALSE) {

    if (!$pula_login) {
      if (empty($this->POST[model_usuario::AD])) {
        throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>Login</b> não pode ficar vazio.");');
      }
      if (isset($this->model_usuario->get(array(model_usuario::AD => $this->POST[model_usuario::AD]))[0])) {
        throw new Exception('show_stack_bar_top("error", "Usuario já utilizado", "O usuário digitado já está cadastrado. Digite outro usuario.");');
      }
      if (empty($this->POST[model_usuario::SENHA])) {
        throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>Senha</b> não pode ficar vazio.");');
      }
      $ad = explode(' ', $this->POST[model_usuario::AD]);
      if (isset($ad[1])) {
        throw new Exception('show_stack_bar_top("error", "Campo inválido", "Campo <b>Login</b> não pode ter espaços entre palavras.");');
      }
    }
    if (empty($this->POST[model_funcionario::EMAIL])) {
      throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>E-mail</b> não pode ficar vazio.");');
    }
    if (empty($this->POST[model_funcionario::EMPRESA])) {
      throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>Empresa</b> não pode ficar vazio.");');
    }
    if (empty($this->POST[model_funcionario::CHAPA])) {
      throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>Chapa</b> não pode ficar vazio.");');
    }
    if (empty($this->POST[model_funcionario::CPF])) {
      throw new Exception('show_stack_bar_top("error", "Campo vazio", "Campo <b>CPF</b> não pode ficar vazio.");');
    }
  }

  public function editar($ID) {
    $this->data['breadcrumb'] = $this->breadcrumb(array('nucleo', 'usuario', 'editar'), array('Núcleo', 'Usuário', 'Editar'));
    $this->data['usuario'] = $this->model_usuario->getUsuario(array(model_usuario::ID => $ID))[0];
    $this->MY_view('nucleo/usuario/editar', $this->data, 'nucleo/commom/header');
  }

  public function atualizar() {
    try {
      $this->validar(TRUE);
      $this->load->Model('model_login');
      $this->load->helper('security');
      $cpf = str_replace('.', '', str_replace('-', '', $this->POST[model_funcionario::CPF]));
      $busca = $this->model_login->getUsuario($this->POST[model_funcionario::CHAPA], $cpf)[0];

      if (is_array($busca)) {

        $campos = array(
            model_funcionario::NOME,
            model_funcionario::CHAPA,
            model_funcionario::CPF,
            model_funcionario::ADMISSAO,
            model_funcionario::FUNCAO,
            model_funcionario::SECAO,
            model_funcionario::DESCSECAO,
            model_funcionario::SITUACAO,
            model_funcionario::TIPO
        );
        $dados = elements($campos, $busca);
        $dados[model_funcionario::EMAIL] = $this->POST[model_funcionario::EMAIL];
        $dados[model_funcionario::EMPRESA] = $this->POST[model_funcionario::EMPRESA];

        if (!empty($this->POST[model_usuario::SENHA])) {
          $campos2 = array(model_usuario::SENHA);
          $dados2 = elements($campos2, $this->POST);
          $dados2[model_usuario::SENHA] = do_hash($dados2[model_usuario::SENHA]);
        } else {
          $dados2 = array();
        }
        $dados2[model_usuario::STATUS] = 'A';
        $dados2[model_usuario::FUNC] = $this->POST[model_funcionario::ID];
      } else {
        throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não encontrado.")');
      }

      $acao = $this->model_funcionario->save($dados, $this->POST[model_funcionario::ID]);
      $acao2 = $this->model_usuario->save($dados2, $this->POST[model_usuario::ID]);


      if ($acao && $acao2) {
        $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Usuário atualizado com sucesso.")');
      } else {
        throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não atualizado.")');
      }

      redirect('nucleo/usuario');
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
      redirect('nucleo/usuario/editar/' . $this->POST[model_usuario::ID]);
    }
  }

  public function desativar() {
    try {
      if (isset($this->POST['id'])) {

        $ID = $this->POST['id'];
        $acao = $this->model_usuario->desativar(array(model_usuario::STATUS => 'D'), $ID);

        if ($acao) {
          $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Usuário desativado com sucesso.")');
        } else {
          throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não desativado.")');
        }
      }
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
    }
    redirect('nucleo/usuario');
  }

  public function ativar() {
    try {
      if (isset($this->POST['id'])) {

        $ID = $this->POST['id'];
        $acao = $this->model_usuario->desativar(array(model_usuario::STATUS => 'A'), $ID);

        if ($acao) {
          $this->session->set_flashdata('MSG', 'show_stack_bar_top("success", "Sucesso", "Usuário ativado com sucesso.")');
        } else {
          throw new Exception('MSG', 'show_stack_bar_top("error", "Erro", "Usuário não ativado.');
        }
      }
    } catch (Exception $exc) {
      $this->session->set_flashdata('MSG', $exc->getMessage());
    }
    redirect('nucleo/usuario');
  }

  public function __destruct() {
    
  }

}
