<?php

if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

class dashboard extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->data['menu_dashboard'] = 'active';
  }

  public function index() {
    $this->data['breadcrumb'] = $this->breadcrumb(array('nucleo', 'dashboard'), array('Núcleo', 'Dashboard'));
    $this->MY_view('nucleo/dashboard', $this->data, 'nucleo/commom/header');
  }

  public function __destruct() {
    
  }

}
