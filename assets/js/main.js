/* Author:
 denner.fernandes - denners777@hotmail.com
 */

var LOCAL = 'http://192.168.1.54/public/assets/';

$(document).ready(function() {

  $('.tooltips').tooltip();
  $('.datatable').dataTable();

  $(".datatable").tablecloth({
    theme: "stats",
    //bordered: true,
    //condensed: true,
    striped: true,
    //sortable: true,
    clean: true,
    cleanElements: "th td",
    customClass: "table table-hover"
  });
  
  $('.datep').datepicker();

});


function deletar($action, $ID, $direct) {

  vex.defaultOptions.className = 'vex-theme-os';

  vex.dialog.confirm({
    message: 'Deseja realmente deletar este registro?',
    className: 'vex-theme-flat-attack',
    callback: function(value) {
      if (value === false) {
        return;
      } else {
        $.ajax({
          type: 'POST',
          url: $action,
          data: 'id=' + $ID,
          context: document.body
        }).done(function() {
          show_stack_bar_top("success", "Sucesso", "Registro deletado com sucesso.");
        }).fail(function() {
          show_stack_bar_top("error", "Error", "Não foi possível deletar registro.");
        });

        setTimeout(function() {
          return $(location).attr('href', $direct);
        }, 3000);
      }
    }
  });
}
function action($action, $ID, $direct, $pergunta, $resposta) {

  vex.defaultOptions.className = 'vex-theme-os';

  vex.dialog.confirm({
    message: 'Deseja realmente '+ $pergunta +' este usuário?',
    className: 'vex-theme-flat-attack',
    callback: function(value) {
      if (value === false) {
        return;
      } else {
        $.ajax({
          type: 'POST',
          url: $action,
          data: 'id=' + $ID,
          context: document.body
        }).done(function() {
          show_stack_bar_top('success', 'Sucesso', 'Usuário '+ $resposta +' com sucesso.');
        }).fail(function() {
          show_stack_bar_top('error', 'Error', 'Não foi possível '+ $pergunta +' esse usuário.');
        });

        setTimeout(function() {
          return $(location).attr('href', $direct);
        }, 3000);
      }
    }
  });
}

function overlay($in) {
  if ($in) {
    $('#overlay').fadeIn('slow');
  } else {
    $('#overlay').fadeOut('slow');
  }
}

function conferir($element) {

  $($element).toggle('slow');

}

function gerarExcel($dados) {
  overlay(true);

  $.post({
    type: 'POST',
    url: LOCAL + 'relatorio/imprimirRelatorio/',
    data: {dados: $dados}
  }).done(function(msg) {
    console.log("Data Saved: " + msg);
  }).fail(function() {
    alert("error");
  });
  overlay(false);
}
